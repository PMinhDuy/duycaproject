import { loadEnv } from 'vite'
import react from '@vitejs/plugin-react'
import pages from 'vite-plugin-pages';
import { defineConfig } from 'vitest/config';


function htmlPlugin(env: Record<string, string | undefined>) {
  return {
    name: 'html-transform',
    transformIndexHtml: {
      enforce: 'pre',
      transform: (html: string) => {
        return html.replace(/<%=(.*?)%>/g, (match, p1) => env[p1] ?? match);
      },
    },
  };
}

// https://vitejs.dev/config/
export default ({mode} : {mode: string}) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd())}
  return defineConfig({
    plugins: [
      react(), 
      pages({
      routeStyle: 'next',
      dirs: 'src/pages',
      importMode: 'async',
    }),],
    server: {
      port: 3000,
    },
  })
}
