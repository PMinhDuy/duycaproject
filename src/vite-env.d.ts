/// <reference types="vite/client" />

/// <reference types="vite-plugin-pages/client-react" />

interface ImportMeta {
    env: {
      ['VITE_APP_TITLE']: string;
      ['VITE_NODE_ENV']: string;
    };
  }

  declare module '*.png' {
    const value: string;
    export default value;
  }
  declare module '*.svg' {
    import React = require('react');
  
    export const ReactComponent: React.FC<React.SVGProps<SVGSVGElement>>;
    const src: string;
    export default src;
  }