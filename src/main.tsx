import React from 'react'
import ReactDOM, { createRoot } from 'react-dom/client'
import App from './App'
import './index.css'
import { BrowserRouter } from 'react-router-dom'


function Main() {
  return(
    <BrowserRouter>
      <App />
    </BrowserRouter>
  )
}
const root = createRoot(document.getElementById('root') as HTMLElement) 
root.render(
  <React.StrictMode>
    <Main />
  </React.StrictMode>,
)

