import { Suspense } from 'react'
import './App.css'
import { Spin } from 'antd'
import { useRoutes } from 'react-router';
import routes from '~react-pages';

function App() {
  return (
    <Suspense fallback={<Spin/>}>
      {useRoutes(routes)}
      <p>Alo</p>
    </Suspense>
  )
}

export default App
